drop table customer if exists;
drop sequence if exists hibernate_sequence;
create sequence hibernate_sequence start with 10 increment by 1;
create table customer (id bigint not null, active boolean, age integer, name varchar(255), primary key (id));
insert into customer (active, age, name, id) values (true, 41, 'Cristian Clever de Oliveira', 9999);
insert into customer (active, age, name, id) values (true, 5, 'Henry William', 9998);
insert into customer (active, age, name, id) values (true, 41, 'Samara Regina', 9997);