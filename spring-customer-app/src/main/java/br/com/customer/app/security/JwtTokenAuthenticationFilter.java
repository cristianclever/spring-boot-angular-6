package br.com.customer.app.security;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

	private String jwtHeaderName;
	private String secretKey;

	public JwtTokenAuthenticationFilter(String jwtHeaderName, String secretKey) {
		this.jwtHeaderName = jwtHeaderName;
		this.secretKey = secretKey;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String token = request.getHeader("authorization");
		
		if(token!=null) {
			token=token.replaceAll("Bearer ", "");  
		}
		
		
	
		try {

			// exceptions might be thrown in creating the claims if for example the token is expired

			
			//Validate the token
			//Claims claims = Jwts.parser().setSigningKey("1234567890".getBytes()).parseClaimsJws(token).getBody();
			
			
			String username = "cristian";
			String authoritie = "ROLE_ADMIN"; 
			
			
			if(false) {
			 Claims claims = Jwts.parser() .setSigningKey("1234567890") .parseClaimsJws(token).getBody();
			 	username = claims.getSubject();
			 	authoritie = (String) claims.get("perfil");
			}
			
			
			if (username != null) {
				@SuppressWarnings("unchecked")
				
				
				SimpleGrantedAuthority sga = new SimpleGrantedAuthority(authoritie);

				// 5. Create auth object
				// UsernamePasswordAuthenticationToken: A built-in object, used by spring to
				// represent the current authenticated / being authenticated user.
				// It needs a list of authorities, which has type of GrantedAuthority interface,
				// where SimpleGrantedAuthority is an implementation of that interface
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, null,Collections.singletonList(sga) );

				// 6. Authenticate the user
				// Now, user is authenticated
				SecurityContextHolder.getContext().setAuthentication(auth);
			}

		} catch (Exception e) {
			// In case of failure. Make sure it's clear; so guarantee user won't be
			// authenticated
			SecurityContextHolder.clearContext();
		}

		// go to the next filter in the filter chain
		filterChain.doFilter(request, response);
	}

}
