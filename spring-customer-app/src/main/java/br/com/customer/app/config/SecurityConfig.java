package br.com.customer.app.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.customer.app.security.JwtTokenAuthenticationFilter;

/**
The prePostEnabled property enables Spring Security pre/post annotations
The securedEnabled property determines if the @Secured annotation should be enabled
The jsr250Enabled property allows us to use the @RoleAllowed annotation
**/

@Configuration
@EnableGlobalMethodSecurity(
		  prePostEnabled = true, 
		  securedEnabled = true, 
		  jsr250Enabled = true)
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

	
	
	@Value("${headerName}")
	private String jwtHeader;
	
	@Value("${secretKey}")
	private String secret;

	
	protected void configure(HttpSecurity http) throws Exception {
 	   http
		.csrf().disable()
		
		// make sure we use stateless session; session won't be used to store user's state.
	 	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) 	
		.and()

		// handle an authorized attempts 
		.exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)) 	
		.and()
		
		
		// Add a filter to validate the tokens with every request
		.addFilterAfter(new JwtTokenAuthenticationFilter(jwtHeader,secret), UsernamePasswordAuthenticationFilter.class)
		
		// authorization requests config
		.authorizeRequests()
		  
		// allow all who are accessing "auth" service
		//   .antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()  
		// must be an admin if trying to access admin area (authentication is also required here)
		//.antMatchers("/gallery" + "/admin/**").hasRole("ADMIN")
		// Any other request must be authenticated
		
		//swagger
		//.antMatchers("/**").permitAll() 
		
		.antMatchers("/actuator/**","/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
		
		
		.antMatchers("/index.html").permitAll()
		.anyRequest().authenticated()
		; 

		//
	}

}
