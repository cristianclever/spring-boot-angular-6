package br.com.customer.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.com.customer.app.advice.ControllerExceptionHandler;
import br.com.customer.app.config.PersistenceJPAConfig;
import br.com.customer.app.controller.CustomerController;
import br.com.customer.app.repository.BaseRepository;
import br.com.customer.app.service.BaseTransactionService;

@SpringBootApplication
@EnableAutoConfiguration
public class CustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
	}

}

