package br.com.customer.app.dto;

public class Retorno {
	
	
	private Retorno subretorno;
	
	private int codigo;
	
	private String descricao;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Retorno getSubretorno() {
		return subretorno;
	}

	public void setSubretorno(Retorno subretorno) {
		this.subretorno = subretorno;
	}
	

}
