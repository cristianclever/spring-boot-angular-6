package br.com.customer.app.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.customer.app.controller.CustomerController;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CustomerController.class);
	
	@ExceptionHandler({ Throwable.class })
	public ResponseEntity<Object> handleConstraintViolation(Throwable e, WebRequest request) {
		
		if(e!=null) {
			log.error("Erro:" + e.getMessage(),e);
		}
		
		return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	@ExceptionHandler({ org.springframework.security.access.AccessDeniedException.class })
	public ResponseEntity<Object> handleConstraintViolation(AccessDeniedException e, WebRequest request) {
		
		if(e!=null) {
			log.error("Erro: [AccessDeniedException]" + e.getMessage(),e);
		}
		
		return new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
	}	
	
		
	
	
	
	@ExceptionHandler({ NullPointerException.class })
	public ResponseEntity<Object> handleConstraintViolation(NullPointerException e, WebRequest request) {
		
		if(e!=null) {
			log.error("Erro:" + e.getMessage(),e);
		}
		
		return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
}
