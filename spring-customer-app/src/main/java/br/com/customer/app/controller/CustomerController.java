package br.com.customer.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.customer.app.dto.Retorno;
import br.com.customer.app.model.Customer;
import br.com.customer.app.service.CustomerService;


@Secured({ "ROLE_VIEWER", "ROLE_EDITOR" , "ROLE_ADMIN"})
@RequestMapping("/api")
@RestController
public class CustomerController extends BaseController{
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	public CustomerService customerService;
	

	
	
	@GetMapping("/x2")
	public Retorno index() {
		
	
		log.info(""+ new Date());

		
		
		Retorno r = new Retorno();
		//r=null;
		r.setCodigo(1);
		r.setDescricao("Teste");
		
		Retorno r2 = new Retorno();
		r2.setCodigo(12);
		r2.setDescricao("Teste2");	
		
		r.setSubretorno(r2);
		
		return r;
	}
	
	
	
	
	
	@GetMapping("/customers")
	public List<Customer> getAllCustomers() {
		System.out.println("Get all Customers...");
 
		List<Customer> customers = new ArrayList<Customer>();
		customerService.findAll().forEach(customers::add);
 
		return customers;
	}
	
	
	@GetMapping("/customers/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable("id") long id) {
		ResponseEntity<Customer> retorno = null;
		try {
			Optional<Customer> customerData = customerService.findById(id);		
			if (customerData.isPresent()) {
				Customer _customer = customerData.get();
				retorno = new ResponseEntity<Customer>(_customer, HttpStatus.OK);
			}		
			else {
				retorno = new ResponseEntity<Customer>((Customer)null, HttpStatus.NOT_FOUND);
			}
		}
		catch(Exception e) {
			retorno = new ResponseEntity<Customer>((Customer)null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
		return retorno;
	}	
	
	
	
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(value = "/customers")
	public Customer postCustomer(@RequestBody @Valid Customer customer) {
 		Customer _customer = customerService.save(new Customer(customer.getName(), customer.getAge()));
		return _customer;
	}
 
	
	
	@DeleteMapping("/customers/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable("id") long id) {
		System.out.println("Delete Customer with ID = " + id + "...");
		
		ResponseEntity<String> retorno = null;
		try {
			customerService.deleteById(id);
			retorno = new ResponseEntity<String>("Customer has been deleted!", HttpStatus.ACCEPTED);
		} catch (EmptyResultDataAccessException e) {
			retorno = new ResponseEntity<String>("Customer has been deleted!", HttpStatus.NO_CONTENT);
		}
		catch(Exception e) {
			retorno = new ResponseEntity<String>("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return retorno;
	}

 
	
	
	
	
	
	@GetMapping(value = "customers/age/{age}")
	public List<Customer> findByAge(@PathVariable int age) {
		List<Customer> customers = customerService.findByAge(age);
		return customers;
	}
	
	
	
	
 

	@PutMapping("/customers/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable("id") long id, @RequestBody Customer customer) {
		ResponseEntity<Customer> retorno = null;
		Customer customerReturn = null;

		try {
			Optional<Customer> customerData = customerService.findById(id);		
			if (customerData.isPresent()) {
				Customer _customer = customerData.get();
				_customer.setName(customer.getName());
				_customer.setAge(customer.getAge());
				_customer.setActive(customer.isActive());
				customerReturn= customerService.save(_customer);
			}		
			else {
				retorno = new ResponseEntity<Customer>(customerReturn, HttpStatus.NO_CONTENT);
			}
		}
		catch(Exception e) {
			retorno = new ResponseEntity<Customer>(customerReturn, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
		return retorno;
	}
}
