package br.com.customer.app.service;

import java.util.List;
import java.util.Optional;

import javax.cache.annotation.CacheResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.customer.app.model.Customer;
import br.com.customer.app.repository.CustomerRepository;

@Service
public class CustomerService extends BaseTransactionService {

	@Autowired
	private CustomerRepository repository;
	
	
	//@CacheResult(cacheName="defaultCache")
	@Transactional(readOnly = true)
	public List<Customer> findAll(){
		List<Customer> listCustomer =  repository.findAll(Sort.by(Order.desc("name")));
		return listCustomer;
		
	}

	
	
	public Customer save(Customer customer) {
		Customer customer_ = repository.save(customer);
		return customer_;
	}



	public void deleteById(long id) throws EmptyResultDataAccessException{
		repository.deleteById(id);
		
	}
	
	
	public List<Customer> findByAge(int age){
		
		return repository.findByAge(age);
	}



	public Optional<Customer> findById(long id) {
		return repository.findById(id);
	}

	
	
	
	
	
	
	
	
}
