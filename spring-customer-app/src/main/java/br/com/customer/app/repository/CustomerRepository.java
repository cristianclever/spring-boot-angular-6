package br.com.customer.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.customer.app.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	public List<Customer> findByAge(int age);
}
