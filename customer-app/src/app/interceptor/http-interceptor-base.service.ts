import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorBaseService implements HttpInterceptor{

  constructor() {
    console.log('HttpInterceptorBaseService!');
  }





  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    console.log('Intercepted!')
    let token: string = localStorage.getItem('token');
    token =  "abc";
    if(!token){
      
    }
    request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });

    return next.handle(request).pipe(
      map( (response:HttpResponse<any>)=>{
        //response = response.clone({statusText:'ok'});
        return response;
      })
    )



    return null;
  }

}
