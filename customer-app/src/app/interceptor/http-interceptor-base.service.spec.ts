import { TestBed } from '@angular/core/testing';

import { HttpInterceptorBaseService } from './http-interceptor-base.service';

describe('HttpInterceptorBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpInterceptorBaseService = TestBed.get(HttpInterceptorBaseService);
    expect(service).toBeTruthy();
  });
});
