import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRoute, Router } from '@angular/router';
import { ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError, Subscription } from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { resolve } from 'q';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseUrl = 'http://localhost:8084/auth/login/?username={{userName}}&password={{password}}';

  private authenticated:boolean = false;

  //Exemplo usando o map
  list():Observable<any>{
    return this.http.get(this.baseUrl)
    .pipe(
      map((e:Response)=> e.json()),
      catchError((e:Response)=> throwError(e))
    );
  }



  public isAhutenticated():boolean{
    return this.authenticated;
  }

  constructor(
    private activatedRoute:ActivatedRoute,
    private router:Router,
    private http: HttpClient
    ) { 

  }

  private map = new Map([['key1', 1], ['key2', 2]]);

  private authenticationResult:any= null;


  public test(){
/*
    let httpRequest = new XMLHttpRequest();


    httpRequest.onreadystatechange = function () {

        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
          
        }
    }
    httpRequest.open(pVerbo, pUrl, false);
    httpRequest.send(null);*/
  }


  public async doLoginx(username:string,password:string):Promise<boolean>{


    // const params = new HttpParams().set('username', username).set('password', password);
     
     let loginURL:string  = `http://localhost:8084/auth/login/?username=${username}&password=${password}`;
     
     console.log("A ")
    const result = await this.http.post<any>(loginURL,{}, { observe: 'response'}).toPromise().then(
       (res:HttpResponse<any>)=>{
        console.log("B ")
          if(res.status == 202){
           this.authenticationResult = res.body;
           this.authenticated = true;
           
         }
       }
     )
     console.log("C")
     const result2 = await this.http.post<any>(loginURL,{}, { observe: 'response'}).toPromise().then(
      (res:HttpResponse<any>)=>{
       console.log("D ")
         if(res.status == 202){
          this.authenticationResult = res.body;
          this.authenticated = true;
          
        }
      }
    )



     console.log("E")
     return this.authenticated ;
   }


  public doLogin(username:string,password:string):Observable<any>{  
    console.log('doLogin');
    let loginURL:string  = `http://localhost:8084/auth/login/?username=${username}&password=${password}`;
    
     /*
     - https://www.concretepage.com/angular/angular-httpclient-post
        observe : 'response' for complete response. 
        observe : 'body' for response with body. 
        observe : 'events' for response with events. 
    */
   const result = this.http.post<any>(loginURL,{}, { observe: 'response'}); 
   return result;
  }


}

/*

.subscribe(
      (res:HttpResponse<any>)=>{
         if(res.status == 202){
          this.authenticationResult = res.body;
          this.authenticated = true;
        }
      }

*/