import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate{

  constructor(
    activatedRouteSnapshot:ActivatedRouteSnapshot,
    routerStateSnapshot:RouterStateSnapshot,
    authenticationService:AuthenticationService
    ) { 

  }



  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) :boolean{

    // this.router.navigate(['login']);
    return true;
  }


}
