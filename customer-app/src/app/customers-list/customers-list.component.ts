import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  public listCustomers:Customer[];


  constructor(private customerService:CustomerService) { }


  public atualizarListagem():void{
    console.log('atualizarListagem');
    this.customerService.getCustomersList().subscribe(
       
        (res:HttpResponse<Customer[]>)=>{
           if(res.status == 200){
            this.listCustomers = res.body;
          }
        },
        (error)=>{
          this.listCustomers=[];
        }
    );


  }



  ngOnInit(    ) {
    this.atualizarListagem();
  }

}
