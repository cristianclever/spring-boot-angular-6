import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  constructor(private customerService:CustomerService,  private route: ActivatedRoute, private router: Router) {

   }


//https://angular.io/guide/router

  //cria uma copia do objeto
  private customer_:Customer;
  private customer:Customer;

   private selectedID:number;
  ngOnInit() {
    this.log("ngOnInit");

    let idd:number = 0;
    this.route.paramMap.subscribe(
      params=>{
        let loadedID:number = +params.get("id");

        //busca o customer
        this.customerService.getCustomer(loadedID).subscribe(
          (c=>{
            console.log(`recebendo customer:' {{c}} `);
            
            
            this.loadCustomer(<Customer>c);
          })
        );


        
      }
    );


  }


  


  private loadCustomer(customer$:Customer):void{


    //obtem o id do customer pela rota
    console.log("Carregando:" + customer$);

    this.customer_ = null;
    if(customer$){
      this.customer = customer$;
      this.customer_ = Object.assign({}, this.customer);
    }
  }

  public save(){
    //envia o dado para o servidor
    this.customerService.updateCustomer(this.customer).subscribe(
      (retorno)=>{
        console.log('Sucesso');
        console.log(retorno);
      },
      (error) =>{
        console.log('ERRO');
        console.log(error);
      }
    )
  }

  public delete(){

   //envia o dado para o servidor
   this.customerService.deleteCustomer(this.customer).subscribe(
    (retorno)=>{
      console.log('Sucesso');
      console.log(retorno);
    },
    (error) =>{
      console.log('ERRO');
      console.log(error);
    }
  )


  }

  public reset(){
    
  }



  ngOnChanges(){
   
  }

  ngDoCheck(){
    this.log("CustomerDetailsComponent.ngDoCheck");
  }




  ngAfterContentInit(){
    this.log("CustomerDetailsComponent.ngAfterContentInit");
  }

  ngAfterContentChecked(){
    this.log("CustomerDetailsComponent.ngAfterContentChecked");
  }
  ngAfterViewInit(){
    this.log("CustomerDetailsComponent.ngAfterViewInit");
  }

  ngAfterViewChecked()
  {
    this.log("CustomerDetailsComponent.nngAfterViewChecked");
  }

  ngOnDestroy(){
    this.log("CustomerDetailsComponent.ngOnDestroy");
  }


  public log(val:string){
    console.log(val);

  }


  public updateCustomer():void{
    this.customerService.updateCustomer(this.customer);
  }

  public  deleteCustomer():void{
    this.customerService.deleteCustomer(this.customer).subscribe(
      ()=>{
        this.customer = null;
      }
      
    );
  }



}
