import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/security/authentication.service';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService:AuthenticationService,private router:Router) { 

  }

  public authenticationResult:any;
  public erroLogin:boolean;
  public username:string = "a";
  public passwordInput:string= "b";
  public remember:boolean = true;

  public redirectSuccess():void{
    this.router.navigate(['customer'])
  }

  public showError():void{
    this.erroLogin = true;
  }

  public doLogin(){
    console.log(this.username + "|" + this.passwordInput)

    this.loginService.doLogin(this.username,this.passwordInput).subscribe(
      (res:HttpResponse<any>)=>{
         if(res.status == 202){
          this.authenticationResult = res.body;
          this.redirectSuccess();
        }
        else{
          this.showError();
        }
      },
      (error)=>{
        console.log(error);
        this.erroLogin = true;
      },
      ()=>{
        console.log('complete')
      }
      
      );

  }

  

  ngOnInit() {
  }

}
