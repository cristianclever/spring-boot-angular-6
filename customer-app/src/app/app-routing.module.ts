import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BlankComponent } from './blank/blank.component';
import { LoginComponent } from './login/login/login.component';

const routes: Routes = [

  { path: '',        component: BlankComponent },
  { path: 'login',        component: LoginComponent },
  { path: 'customer',        component: CustomersListComponent, children:[
      { path: '',    component: PageNotFoundComponent },
      { path: ':id',    component: CustomerDetailsComponent }
    ]
},




  
 
  { path: '**',    component: PageNotFoundComponent }
// { path: '',   redirectTo: '/heroes', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
