import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from './customer';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { catchError, map, tap, concatMap, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseUrl = 'http://localhost:8083/customer-app/api/customers';
 





  constructor(private http: HttpClient) {

  }
  
  

   //ok
   public getCustomersList(): Observable<any> {
    const params = new HttpParams().set('q', 'cironunes');
    return this.http.get<Response>(this.baseUrl,{params, observe: 'response', responseType: "json" });
  }

 
  public deleteCustomer(_customer: Customer): Observable<any> {

    if(!_customer){
      let e:Error = new Error("Customer incorreto");
      throw Observable.throw(e); 
    }
  
    let id: number = _customer.id
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }


























   public getCustomer(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`).pipe(
 
    );
  }
 
  public createCustomer(customer: Customer): Observable<Object> {
    return this.http.post(this.baseUrl, customer);
  }
 
  public updateCustomer(_customer: Customer): Observable<Object> {
    let id: number = _customer.id
    return this.http.put(`${this.baseUrl}/${id}`, _customer);
  }

 
  
 
  public getCustomersByAge(age: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/age/${age}`);
  }
 
  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }   




}
