import { Component } from '@angular/core';
import { CustomerService } from './customer.service';
import { Customer } from './customer';
import { HttpResponse, HttpResponseBase } from '@angular/common/http';
import { interval } from 'rxjs';
import { AuthenticationService } from './security/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'customer-app';

  public constructor(
    private customerService:CustomerService,
    private authenticationService:AuthenticationService
    ){
  }


  public populaCustomers(customers:Customer[],error:Error):void{

  }

  public selectedCustomer:Customer;

  public idd:number = 0;



  ngOnInit(){
    console.log("AppComponent.onInit");
  }
}
